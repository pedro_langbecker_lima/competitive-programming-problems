#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 10963 - The Swallowing Ground
// Pedro Langbecker Lima resolution - plima@inf.ufsm.br
//link: https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1904

int main(){
    int a;
    scanf("%d", &a);
    for(int i=0; i < a; i++){
        int b;
        scanf("%d", &b);
        int sz;
        int x,y;
        scanf("%d %d", &x,&y);
        sz = x - y;
        bool end=true;
        for (int j=0; j< b-1; j++){
            scanf("%d %d", &x,&y);
            if (x - y != sz)
                end = false;
        }
        if (i > 0)
            printf("\n");

        if (end)
            printf("yes\n");
        else
            printf("no\n");
    }
    return 0;
}
