#include <stdio.h>

int main(){
    int m,nr;
    float pi,emp;

    while (scanf("%d %f %f %d", &m,&pi,&emp,&nr), m > 0){
        float cop = emp;
        int n;
        float dep;
        float aux;
        int mes = 0;
        for (int i=0; i < nr; i++){
            scanf("%d %f", &n, &dep);
            if (i == nr - 1){
                while(emp <= cop){
                    emp *= 1 - aux;
                    cop -= pi;
                    mes++;
                }
            }
            else if (n > i)
                for (int j=0; j < n - i; j++){
                    emp *= 1 - aux;
                    cop -= pi;
                    printf("emp:%f cop:%f\n",emp, cop);
                    if (emp <= cop)
                        mes++;
                }
            else{
                emp *= 1 - dep;
                aux = dep;
                if (i > 0)
                    cop -= pi;
                printf("emp:%f cop:%f\n",emp, cop);
                if (emp <= cop)
                    mes ++;
            }
        }
        printf("%d months\n", mes);
    }
}
