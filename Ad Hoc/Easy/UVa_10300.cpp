#include <iostream>
#include <cstdio>
#include <utility>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long       ll;
typedef pair<int, int>  ii;
typedef vector<ii>      vii;
typedef vector<int>     vi;
#define INF 1000000000

// UVa 10300 - Ecological Premium
// Pedro Langbecker Lima resolution - plima@inf.ufsm.br
//link: https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1241

int main(){
    int a;
    scanf("%d", &a);
    for (int i=0; i< a; i++){
        int b;
        int v = 0;
        scanf("%d", &b);
        for (int j=0; j < b; j++){
            int x,y,z;
            scanf("%d %d %d", &x,&y,&z);
            v += x*z;
        }
        printf("%d\n",v);

    }

    return 0;
}
