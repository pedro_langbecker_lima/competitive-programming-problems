#include <stdio.h>

int main(){
    char c;
    bool aux=true;

    while (scanf("%c", &c) != EOF){
        if (c == '"'){
            if (aux) printf("``");
            else printf("''");
            aux = !aux;
        }
        else printf("%c",c);
    }

}
