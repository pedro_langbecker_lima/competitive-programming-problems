
#include <cstdio>
#include <algorithm>

using namespace std;

int main(){
    int i, j;

    while (scanf ("%d %d", &i, &j) != EOF){
        int tempi = i, tempj = j;
        int max = 0;
        if (i > j)
            std::swap(i,j);

        for (int x = i; x <= j; x++){
            int x2 = x;
            int cont = 1;
            while (x2 != 1){
                if (x2 % 2 != 0)
                    x2 = 3*x2 + 1;
                else
                    x2 >>= 1;
                cont ++;
            }

          if (cont > max)
            max = cont;
        }
        printf("%d %d %d\n", tempi, tempj, max);

  }



}
