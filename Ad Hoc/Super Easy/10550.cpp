#include <stdio.h>


int main (){
    int a,b,c,d;
    int deg;
    while (scanf("%d %d %d %d", &a,&b,&c,&d), a || b || c || d){
        deg = 1080;
        deg += (a > b) ? (a - b)*9 : (a - b + 40)*9;
        deg += (c > b) ? (c - b)*9 : (c - b + 40)*9;
        deg += (c > d) ? (c - d)*9 : (c - d + 40)*9;
        printf ("%d\n", deg);
    }
}
