#include <stdio.h>

int main(){
    int x;
    int j=0;
    while (scanf("%d", &x), x != 0){
        int n;
        int count = 0;
        for (int i=0; i<x; i++){
            scanf("%d", &n);
            if (n == 0)
                count --;
            else
                count++;
        }
        printf("Case %d: %d\n", j+1, count);
        j++;
    }

}
