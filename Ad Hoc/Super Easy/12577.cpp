#include <stdio.h>
#include <string.h>

int main(){
    int i=0;
    char a[20];
    while (scanf("%s", a), a[0] != '*'){
        
        if (!strcmp(a,"Hajj"))
            printf("Case %d: Hajj-e-Akbar\n", i+1);
        else if(!strcmp(a,"Umrah"))
            printf("Case %d: Hajj-e-Asghar\n", i+1);
        i++;
    }
}
