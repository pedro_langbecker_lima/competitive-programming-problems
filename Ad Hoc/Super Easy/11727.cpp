#include <stdio.h>
#include <stdlib.h>

int compare (const void * a, const void *b){
    return ( *(int*) a - *(int*) b);
}

int main(){
    int n;
    scanf("%d",&n);


    for (int i=0; i < n; i++){
        int vet[3];
        scanf("%d %d %d", &vet[0],&vet[1],&vet[2]);
        qsort(vet,3, sizeof(int), compare);
        printf("Case %d: %d\n", i+1, vet[1]);
    }

}
