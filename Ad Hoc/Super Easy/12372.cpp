#include <stdio.h>

int main(){
    int n;
    scanf("%d", &n);
    for (int i=0; i < n; i++){
        int a,b,c;
        scanf("%d %d %d",&a,&b,&c);
        if (a <= 20 && b <= 20 && c <= 20)
            printf("Case %d: good\n", i+1);
        else
            printf("Case %d: bad\n", i+1);
    }
}
