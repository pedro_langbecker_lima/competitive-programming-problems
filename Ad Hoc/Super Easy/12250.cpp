#include <stdio.h>
#include <string.h>

int main(){
    char a[20];
    const char* b[] = {"ENGLISH", "SPANISH", "GERMAN", "FRENCH", "ITALIAN", "RUSSIAN"};
    const char* c[] = {"HELLO", "HOLA", "HALLO", "BONJOUR", "CIAO", "ZDRAVSTVUJTE"};
    int j=0;
    while (1){
        scanf("%s", a);
        if (a[0] == '#')
            break;
        bool acho=false;
        for (int i=0; i < 6; i++)
            if (strcmp(a, c[i]) == 0){
                printf("Case %d: %s\n",j+1, b[i]);
                acho = true;
                break;
            }
        if (!acho)
            printf("Case %d: UNKNOWN\n", j+1);
        j++;
    }

}
