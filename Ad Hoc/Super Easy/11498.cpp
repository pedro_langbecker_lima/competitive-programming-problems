#include <stdio.h>

int main(){
    int k, m,n;
    while (scanf("%d",&k), k > 0){
        scanf("%d %d", &m,&n);
        for (int i=0; i < k; i++){
            int a,b;
            scanf("%d %d", &a,&b);
            if (a == m || b == n)
                printf("divisa\n");
            else if (a > m && b > n)
                printf("NE\n");
            else if (a > m && b < n)
                printf("SE\n");
            else if (a < m && b > n)
                printf("NO\n");
            else if (a < m && b < n)
                printf("SO\n");
        }
    }
}
